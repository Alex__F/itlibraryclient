import Vue from 'vue';
import Vuex from 'vuex';
import actions from './actions';

Vue.use(Vuex);

const state = {
  allBooks: [],
  currentCategory: 'all',
  showMainApp: true,
  showLogin: false,
  currentPage: 1,
  searchText: ''
};

const mutations = {
  setAllBooks (state, {books}) {
    state.allBooks.push(...books);
  },
  setSearchBooks (state, {books}) {
    state.allBooks = books;
  },
  setBooksByCategory (state, {books}) {
    state.allBooks = books;
  },
  setCurrentCategory (state, category) {
    state.currentCategory = category.categoryId;
  },
  setShowMainApp (state) {
    state.showMainApp = !state.showMainApp;
  },
  setShowLogin (state) {
    state.showLogin = !state.showLogin;
  },
  setCurrentPage (state) {
    state.currentPage++;
  },
  setSearchText (state, {text}) {
    state.searchText = text;
  }
};

export default new Vuex.Store({
  state,
  mutations,
  actions,
  strict: process.env.NODE_ENV !== 'production'
});
