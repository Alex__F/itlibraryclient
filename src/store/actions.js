/**
 * Created by alexeyformanyuk on 13.02.17.
 */

import axios from 'axios';
import config from '../config.js';

export default {
  getAllBooks({commit}) {
    axios.get(`http://${config.devAddress}/api/book/`, {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
      },
      params: {
        skip: 0,
        limit: 16
      }
    })
      .then(({data}) => commit({type: 'setAllBooks', books: data}))
      .catch(err => {throw err});
  },

  searchBook({commit, state}) {
    const {searchText, currentCategory} = state;
    const sendParams = Object.assign({search: searchText}, {skip: 0, limit: 16},
      currentCategory === 'all' ? null : {categoryIds: currentCategory});

    axios.get(`http://${config.devAddress}/api/book`, {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
      },
      params: sendParams
    })
      .then(({data}) => commit({type: 'setSearchBooks', books: data}))
      .catch(err => {throw err});
  },

  getCategories() {
    return new Promise((resolve, reject) => {
      axios.get(`http://${config.devAddress}/api/category`, {
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
        }
      })
        .then(({data}) => resolve(data))
        .catch(err => reject(err));
    });
  },

  getBookByCategory({commit}, categoryId) {
    commit({type: 'setCurrentCategory', categoryId});

    axios.get(`http://${config.devAddress}/api/book`, {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
      },
      params: {
        categoryIds: categoryId === 'all' ? '' : categoryId
      }
    })
      .then(({data}) => commit({type: 'setBooksByCategory', books: data}))
      .catch(err => {throw err})
  },

  getMoreBooks({commit, state}) {
    const {currentPage, searchText} = state;

    axios.get(`http://${config.devAddress}/api/book/`, {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
      },
      params: {
        search: searchText,
        skip: currentPage * 16,
        limit: 16
      }
    })
      .then(({data}) => commit({type: 'setAllBooks', books: data}))
      .catch(err => {throw err});
  }
};
